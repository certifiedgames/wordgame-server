package certifiedgames.wordgame;

public class Main {
    public static void main(String[] args) {

        //Any initialization code must happen before socket server starting, as it will block the main thread.
        new SocketServer(53244).start();
    }
}
