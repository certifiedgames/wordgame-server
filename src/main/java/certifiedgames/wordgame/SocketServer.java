package certifiedgames.wordgame;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class SocketServer {
    private int port;
    public ArrayList<SocketConnectionThread> clients = new ArrayList<SocketConnectionThread>();

    public SocketServer(int port) {
        this.port = port;
    }

    public void start() {
        System.out.println("Starting socket server...");
        try {
            ServerSocket serverSocket = new ServerSocket(port);
            System.out.println("Server Created.");
            while (true) {
                System.out.println("Looking for clients...");
                Socket clientSocket = serverSocket.accept();
                System.out.println("Client found - creating thread...");
                SocketConnectionThread clientThread = new SocketConnectionThread(clientSocket);
                clients.add(clientThread);
                clientThread.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
