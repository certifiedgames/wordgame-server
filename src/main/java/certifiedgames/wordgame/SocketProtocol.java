package certifiedgames.wordgame;

/**
 * An instance of SocketProtocol is created by each connection thread to handle input.
 */
public class SocketProtocol {

    /**
     * Process input received from a socket client. This method will run on the thread of the socket connection.
     *
     * @param input Input received from socket client.
     * @return Text to return to client; return null to return nothing; return `stop` to kill connection.
     */
    public String processInput(String input) {
        //TODO - Handle input from clients.

        return null;
    }
}
