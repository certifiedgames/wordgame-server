package certifiedgames.wordgame;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class SocketConnectionThread extends Thread {
    private PrintWriter out;
    private BufferedReader in;


    public SocketConnectionThread(Socket connection) {
        try {
            out = new PrintWriter(connection.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        String inputLine, outputLine;

        SocketProtocol protocol = new SocketProtocol();
        try {
            while ((inputLine = in.readLine()) != null) {
                outputLine = protocol.processInput(inputLine);
                if (outputLine != null) {
                    if (outputLine.equals("stop"))
                        break;
                    out.println(outputLine);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();

            System.out.println("Killing client thread due to IOException...");
        }

    }

    public void send(String msg) {
        out.println(msg);
    }
}
